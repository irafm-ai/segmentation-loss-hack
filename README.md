### A repository of paper '*Image segmentation losses with modules expressing a relationship between predictions*' submitted to IPMU 2022.

The file [modules_and_example.ipynb](modules_and_example.ipynb) contains the implementation of the two proposed modules and shows how they can be integrated into a loss function.

---

**Abstract**:

We focus on the semantic image segmentation with the usage of deep neural networks, with emphasis on the loss functions used for training the networks. Considering region-based losses - Dice loss and Tversky loss - we propose two independent modules that easily modify the loss functions to take into account the relationship between the classes' predictions and increase the slope of the gradient. The first module expresses the ambiguity between classes and the second module utilizes a differentiable soft argmax function. Each of the modules is used before the standard loss is computed and remains untouched. In the benchmark, we demonstrate the usefulness of our modules by improving the IOU and F1 coefficients on the test dataset for all tested scenarios. The proposed modules are easy to integrate into existing solutions and add near-zero computation overhead.
